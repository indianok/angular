import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ApplyComponent } from './components/apply/apply.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { ArchiveComponent } from './components/archive/archive.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'apply', component: ApplyComponent },
  { path: 'contact', component: ContactUsComponent },
  { path: 'archive', component: ArchiveComponent },
  { path: '**', component: PageNotFoundComponent },
];
