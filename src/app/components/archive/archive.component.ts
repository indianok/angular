import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatCardModule } from '@angular/material/card';

@Component({
  selector: 'app-archive',
  standalone: true,
  imports: [MatCardModule],
  templateUrl: './archive.component.html',
  styleUrl: './archive.component.scss',
})
export class ArchiveComponent {
  constructor(public sanitizer: DomSanitizer) {}

  videos: Video[] = [
    {
      name: 'Pine Hills (Vácrátót)',
      year: 2007,
      url: 'https://www.youtube-nocookie.com/embed/Wvgvp2b8LZc?si=p0GlQ5AHgt0xixQq',
    },
    {
      name: 'Water Valley (Piliscsaba)',
      year: 2008,
      url: 'https://www.youtube-nocookie.com/embed/gtMvvhKqnsA?si=JuVdCaFGSrTyu-w4',
    },
    {
      name: 'Sand Hills',
      year: 2009,
      url: 'https://www.youtube-nocookie.com/embed/lTpkuQDsOLY?si=I9rClZzkdkRi0kQr',
    },
    {
      name: 'Nagy Indián Tábor',
      year: 2011,
      url: 'https://www.youtube-nocookie.com/embed/tRL0a85yOKo?si=JmQGrT3cL4SxgYi2',
    },
  ];
}

type Video = {
  name: string;
  year: number;
  url: string;
};
