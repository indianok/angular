import { Component } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'app-contact-us',
  standalone: true,
  imports: [MatCardModule, MatButtonModule, MatIconModule],
  templateUrl: './contact-us.component.html',
  styleUrl: './contact-us.component.scss',
})
export class ContactUsComponent {
  people: Person[] = [
    {
      profile_image_url: '/assets/images/jobarat.jpg',
      name: 'Moldován László (Jóbarát)',
      role: 'Táborszervező',
      description: [
        '59 éves vagyok, Budapesten élek.',
        'A végzettségem kulturális antropológus, szociológus, pedagógus.',
        '45 éve foglalkozom az indián kultúrával, szokásokkal, hagyományokkal.',
        '18 éven keresztül magam is aktív résztvevõje voltam különbözõ indián táboroknak.',
        'Gyerekek számára 1989.-óta szervezünk táborokat.',
        'Munkatársaim, barátaim szintén gyakorlott táboroztatók és az indián hagyományok tudói.',
      ],
      phone_number: '+36 30 380 7974',
      email_address: 'sels@freemail.hu',
    },
    {
      profile_image_url: '/assets/images/medve.jpg',
      name: 'Nagy Mátyás (Éneklő Medve)',
      role: 'Táborvezető',
      description: [
        '31 éves vagyok. / idén lesz a 14. táborom :) /',
        '12 évesen, kölyökként kerültem az első, Jóbarát által szervezett indiántáborba.',
        'Az azóta eltelt években, minden nyáron lejárok táborozni.',
        'Tavaly már a 6 éves kisfiunkkal Péterrel (Üvöltő Farkas) voltunk a táborban.',
        'Mindig is szerettem gyerkekkel foglalkozni, és a nomád életmód szerint élni.',
        'Az elsősegély végzettségem révén a táborban én vagyok a lehorzsolt térdek, és az ujjakba ment szálkák gyógyítója.',
        'Hobbim az olvasás, a túrázás, a természetjárás. 21 éve öcsém is lelkes tagja törzsünknek.',
      ],
    },
  ];
}

type Person = {
  profile_image_url: string;
  name: string;
  role: string;
  description: string[];
  phone_number?: string;
  email_address?: string;
};
